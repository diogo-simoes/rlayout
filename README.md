# RLayout #
### Responsive layout for portal based webapps ###

Try out the live demo here: [codepen.io/diogo-simoes/](http://codepen.io/diogo-simoes/full/WwRxOj/)   
Try different viewport sizes to see how the layout behaves in different devices.

Credits also go for [Tiago Pratas](https://www.linkedin.com/in/tiago-pratas-350b1328/en) ([tiagopratas.pt](http://tiagopratas.pt)) for some additional styling and his pixel keen eye.   
Thanks mate! ;)