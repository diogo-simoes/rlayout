
/*
 *  RLayout
 *  =======
 *
 *  Responsive layout for portal based webapp
 * 
 *  (c) 2015-2016 Diogo Simoes - diogosimoes.com
 *
 */

$( document ).ready( function () {

	/**************
	 *  USER-MENU
	 **************/
	 var handleClickAnywhere = function (evt) {
		$( document ).off('click', handleClickAnywhere);
		$('#user-menu-toggler').trigger('click');
	}
	$('#user-menu-toggler').on('click', function () {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
		} else {
			$(this).addClass('open');
		}
		$('.user-menu').slideToggle(250);
	});
	
	$('.user-menu').on('mouseleave', function (evt) {
		$( document ).on('click', handleClickAnywhere);
	});
	$('.user-menu').on('mouseenter', function (evt) {
		$( document ).off('click', handleClickAnywhere);
	});
	$('#user-menu-toggler').on('mouseleave', function (evt) {
		if ($(this).hasClass('open')) {
			$( document ).on('click', handleClickAnywhere);
		}
	});
	$('#user-menu-toggler').on('mouseenter', function (evt) {
		if ($(this).hasClass('open')) {
			$( document ).off('click', handleClickAnywhere);
		}
	});


	/**************
	 *   PORTALS
	 **************/
	 $('.portal-small-selected').text($('.selected-portal').text());
	 $('.portal-small-selected').on('click', function () {
	 	if ($('.portal-small-selected').hasClass('open')) {
	 		$('.portal-small-selected').removeClass('open');
	 		$('.portals').removeClass('open');
	 	} else {
			$('.portal-small-selected').addClass('open');
			$('.portals').addClass('open');
	 	}
	 });
	 $('.portal').click( function () {
		$('.selected-portal').removeClass('selected-portal');
		$(this).addClass('selected-portal');
		$('.portal-small-selected').text($(this).text());
		$('.portal-small-selected').removeClass('open');
		$('.portals').removeClass('open');
	});


	/**************
	 *    MENU
	 **************/
	var originalMenuHeight = $('.menu').height();
	var originalContainerHeight = $('.app-container').height();
	var calcMenuHeight = function () {
		$('.menu').css('height', originalMenuHeight);
		$('.app-container').css('height', originalContainerHeight);
		var menuH = $( document ).height() - $('#status-bar').height() - $('#header').height();
		$('.menu').css('height', menuH);
		var containerH = menuH + $('#status-bar').height() + $('#header').height();
		$('.app-container').css('height', containerH);
	};

	calcMenuHeight();
	$(window).on('resize', function () {
		calcMenuHeight();
	});

	$('.menu-toggler').on('click', function () {
		if ($('.menu').hasClass('menu-collapsed')) {
			//Menu
			$('.menu').removeClass('menu-collapsed');
			$('.menu').addClass('menu-expanded');
			//Menu toggler icon
			$('.menu-toggler-icon').removeClass('fa-chevron-down');
			$('.menu-toggler-icon').addClass('fa-chevron-up');
			//Main content
			$('#main').removeClass('expanded');
			//Footer
			$('.app-footer').removeClass('expanded');
		} else {
			//Menu
			$('.menu').removeClass('menu-expanded');
			$('.menu').addClass('menu-collapsed');
			//Menu toggler icon
			$('.menu-toggler-icon').removeClass('fa-chevron-up');
			$('.menu-toggler-icon').addClass('fa-chevron-down');
			//Main content
			$('#main').addClass('expanded');
			//Footer
			$('.app-footer').addClass('expanded');
		}
	});
	

	/**************
	 * MENU-ITEMS
	 **************/
	var menu;
	/* 
	 * TODO: Instead of using Mnodes only for internal representation,
	 * 			have it encapsulating the Menu component (together with
	 *			the conventioned classes). Manually and explicitly dealing
	 *			with the events is too messy and error prone.
	 */

	function buildMenu () {
		var menu = new Mnode();
		var iter = $('.menu-items li').first();
		var levels = {
			'menu-item-l0': 1,
			'menu-item-l1': 2,
			'menu-item-l2': 3,
			'menu-item-l3': 4
		}
		var lastLvl = 0;
		var visitor = [menu];
		while (iter.hasClass('menu-item')) {
			var lvl;
			if (iter.hasClass('menu-item-l0')) {
				lvl = levels['menu-item-l0'];
			} else if (iter.hasClass('menu-item-l1')) {
				lvl = levels['menu-item-l1'];
			} else if (iter.hasClass('menu-item-l2')) {
				lvl = levels['menu-item-l2'];
			} else if (iter.hasClass('menu-item-l3')) {
				lvl = levels['menu-item-l3'];
			}

			if (lvl > lastLvl) {
				// Increments in tree lvl are always by 1
				var parent = visitor.pop();
				visitor.push(parent);

				var child = parent.addNode(iter);
				visitor.push(child);
			} else if (lvl === lastLvl) {
				var sibling = visitor.pop();
				var parent = visitor.pop();
				visitor.push(parent);

				var child = parent.addNode(iter);
				visitor.push(child);
			} else if (lvl < lastLvl) {
				// Decrements in tree lvl can be by more than 1
				visitor = visitor.slice(0, (lvl-lastLvl-1));
				var parent = visitor.pop();
				visitor.push(parent);

				var child = parent.addNode(iter);
				visitor.push(child);
			}

			lastLvl = lvl;
			iter = iter.next();
		}
		return menu;
	}

	function drawMenu(menu) {
		menu.applyDown(false, function () {
			this.hide();
		});
		menu.applyChildren( function () {
				this.find('.menu-header-icon').removeClass('fa-minus-circle');
				this.find('.menu-header-icon').addClass('fa-plus-circle');
		});
		menu.find($('.selected-menu-item').first()).applyRelatives( function () {
				if (menu.find(this).hasChildren()) {
					this.find('.menu-item-icon').removeClass('fa-chevron-down');
					this.find('.menu-item-icon').addClass('fa-chevron-left');
				}
				this.show();
		});
		menu.find($('.selected-menu-item').first()).applyChildren(function () {
				if (menu.find(this).hasChildren()) {
					this.find('.menu-item-icon').removeClass('fa-chevron-down');
					this.find('.menu-item-icon').addClass('fa-chevron-left');
				}
				this.show();
			});
		menu.find($('.selected-menu-item').first()).applyUp(true, function () {
				this.find('.menu-header-icon').removeClass('fa-plus-circle');
				this.find('.menu-header-icon').addClass('fa-minus-circle');
				if (menu.find(this).hasChildren()) {
					this.find('.menu-item-icon').removeClass('fa-chevron-left');
					this.find('.menu-item-icon').addClass('fa-chevron-down');
				}
				if (this.hasClass('menu-item menu-item-l0')) {
					this.addClass('selected-menu-item');
				}
		});
		calcMenuHeight();
	}

	$('.menu-item:not(:has(a))').click( function () {
		if ($(this).find('.menu-header-icon').hasClass('fa-plus-circle')) {
			// Expand
			$(this).find('.menu-header-icon').removeClass('fa-plus-circle');
			$(this).find('.menu-header-icon').addClass('fa-minus-circle');

			if ($(this).hasClass('selected-menu-item')) {
				var tiny = $('.selected-menu-item').not($(this));
				menu.find(tiny).applyRelatives( function () {
					this.slideDown(300, function () {calcMenuHeight();});
				});
			} else {
				menu.find($(this).first()).applyChildren( function () {
					this.slideDown(300, function () {calcMenuHeight();});
					if (menu.find(this).hasChildren()) {
						this.find('.menu-item-icon').removeClass('fa-chevron-down');
						this.find('.menu-item-icon').addClass('fa-chevron-left');
					}
				});
			}
		} else {
			// Collapse
			$(this).find('.menu-header-icon').removeClass('fa-minus-circle');
			$(this).find('.menu-header-icon').addClass('fa-plus-circle');
			menu.find($(this).first()).applyDown(false, function () {
					this.slideUp(200, function () {calcMenuHeight();});
			});
		}
	});
	$('.menu-item:has(a)').click( function () {
		$('.selected-menu-item').removeClass('selected-menu-item');
		$(this).addClass('selected-menu-item');
		drawMenu(menu);
	});

	menu = buildMenu();
	drawMenu(menu);
	
	window.menu = menu;
	
	/*
	 * Faux-behavior
	 */
	var changeTheme = function () {
		var theme = $('.theme-builder select option:selected').val();
		$('#stylesheet').attr('href', 'styles/' + theme + '.css');
	};
	$('.theme-builder select').change(changeTheme);
});

function Mnode (jqnode, parent) {
	this.jqnode = jqnode ;
	this.children = [];
	this.parent = parent;
}

Mnode.prototype.addNode = function (node) {
	var mnode = new Mnode(node, this);
	this.children.push(mnode);
	return mnode;
}

Mnode.prototype.isLeaf = function () {
	return this.children.length === 0;
}

Mnode.prototype.isRoot = function () {
	return !(this.parent) && true;
}

Mnode.prototype.hasInternal = function () {
	return (this.jqnode) && true;
}

Mnode.prototype.applyUp = function (toself, fn) {
	if (toself && this.jqnode) {
		fn.apply(this.jqnode);
	}
	if (this.parent) {
		this.parent.applyUp(true, fn);
	}
}

Mnode.prototype.applyDown = function (toself, fn) {
	if (toself && this.jqnode) {
		fn.apply(this.jqnode);
	}
	if (!this.isLeaf()) {
		this.children.forEach( function (el, id, ar) {
			el.applyDown(true, fn);
		});
	}
}

Mnode.prototype.applyChildren = function (fn) {
	if (!this.isLeaf()) {
		this.children.forEach( function (el, id, ar) {
			if (el.jqnode) {
				fn.apply(el.jqnode);
			}
		});
	}
}

Mnode.prototype.getSiblings = function () {
	var siblings = [];
	if (this.parent) {
		siblings = this.parent.children.slice();
		for (i = 0; i < siblings.length; i++) {
			if (siblings[i] === this) {
				siblings.splice(i,1);
				break;
			}
		}
	}
	return siblings;
}

Mnode.prototype.applyRelatives = function (fn) {
	if (this.jqnode) {
		fn.apply(this.jqnode);
	}
	if (this.parent) {
		var siblings = this.getSiblings();
		for (i = 0; i < siblings.length; i++) {
			if (siblings[i].jqnode) {
				fn.apply(siblings[i].jqnode)
			}
		}
		this.parent.applyRelatives(fn);
	}
}

Mnode.prototype.find = function (jqnode) {
	if (this.jqnode && this.jqnode.is(jqnode)) {
		return this;
	}
	for (var i = 0; i < this.children.length; i++) {
		var found = this.children[i].find(jqnode);
		if (found) {
			return found;
		}
	}
}

Mnode.prototype.isDescendant = function (jqnode) {
	if (this.parent && this.parent.jqnode && this.parent.jqnode.is(jqnode)) {
		return true;
	}
	if (this.parent) {
		return this.parent.isDescendant(jqnode);
	}
	return false;
}

Mnode.prototype.hasChildren = function (jqnode) {
	return this.children.length > 0;
}