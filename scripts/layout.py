
#
#  RLayout
#  =======
#
#  Responsive layout for portal based webapp
# 
#  (c) 2015-2016 Diogo Simoes - diogosimoes.com
#

import json
import pystache

templateReader = open("../layout-template.html", 'r')
contentReader =  open("layout-content.json", 'r')
layoutWriter = open("../layout.html", 'w')

layoutTemplate = templateReader.read().decode('utf-8')
contentJson = json.load(contentReader)


layout = pystache.render(layoutTemplate, contentJson)
layoutUTF = layout.encode('utf-8')
layoutWriter.write(layoutUTF)

templateReader.close()
contentReader.close()
layoutWriter.close()